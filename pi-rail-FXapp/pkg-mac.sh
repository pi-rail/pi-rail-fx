#!/bin/bash

file="./res/version.properties"
function prop {
    grep "${1}" ${file} | cut -d'=' -f2
}
VERSION=$(prop 'application.version')

/Library/Java/JavaVirtualMachines/jdk-21.0.5-full.jdk/bin/jpackage --input ./bin/ --name CTC-App --app-version $VERSION --main-jar CTC-Desktop.jar --mac-package-name "CTC-App" --icon ctc-icon.icns --vendor "CTC-System.de" --type dmg \
--mac-sign --mac-package-signing-prefix "de.pidata.rail.fx" --mac-signing-keychain "$HOME/Library/Keychains/login.keychain-db" --mac-signing-key-user-name "PI-Data AG" --verbose

xcrun notarytool submit "CTC-App-$VERSION.dmg" --keychain-profile "pi-data-dev" --wait