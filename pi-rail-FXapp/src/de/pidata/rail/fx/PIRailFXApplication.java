package de.pidata.rail.fx;

import de.pidata.file.FilebasedRollingLogger;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.docking.PIDockingFrameworkBase;
import de.pidata.gui.docking.PIDockingGenerator;
import de.pidata.gui.docking.PIDockingDialog;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.guidef.ControllerFactory;
import de.pidata.gui.javafx.FXApplication;
import de.pidata.log.Level;
import de.pidata.log.Logger;
import de.pidata.models.tree.BaseFactory;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.rail.railway.RailwayFactory;
import de.pidata.rail.track.PiRailTrackFactory;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;
import de.pidata.system.desktop.DesktopSystem;
import goryachev.common.util.GlobalSettings;
import goryachev.fx.internal.LocalSettings;
import goryachev.fxdock.FxDockFramework;
import goryachev.fxdock.internal.FxDockSchema;
import javafx.scene.Node;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;

public class PIRailFXApplication extends FXApplication {

  public static final String SETTINGS_CONF = "pi-rail.settings.conf";

  public DialogController getRootDialogController() {
    return rootDlgCtrl;
  }
  public void init() throws Exception {

    super.init();

    try {
      File jarFile = new File( getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
      this.basePath = jarFile.getParentFile().getAbsolutePath();
    }
    catch (URISyntaxException e) {
      // do nothing - basePath stays "."
    }
    Platform.loadServices = false;
    new FXFactoryPIRail(); // replace UI Factory
    try {
      File appDir = new File( DesktopSystem.getPathAppData(), SystemManager.getInstance().getProgramName() );
      File logDir = new File( appDir, "logs" );
      logDir.mkdirs();
      File logFile = new File( logDir, "pi-rail.log" );
      FilebasedRollingLogger streamLogger = new FilebasedRollingLogger( logFile.getAbsolutePath(), 1, 30, Level.DEBUG );
      Logger.addLogger( streamLogger );
      SystemManager.getInstance().addReportFileOwner( streamLogger );
    }
    catch (Exception ex) {
      Logger.error( "Error creating log file", ex );
    }

    ModelFactoryTable factoryTable = ModelFactoryTable.getInstance();
    factoryTable.getOrSetFactory( BaseFactory.NAMESPACE, BaseFactory.class );
    factoryTable.getOrSetFactory( ControllerFactory.NAMESPACE, ControllerFactory.class );
    factoryTable.getOrSetFactory( PiRailFactory.NAMESPACE, PiRailFactory.class );
    factoryTable.getOrSetFactory( PiRailTrackFactory.NAMESPACE, PiRailTrackFactory.class );
    factoryTable.getOrSetFactory( RailwayFactory.NAMESPACE, RailwayFactory.class );
  }

  @Override
  public void start( Stage primaryStage ) throws Exception {

    // Plug in custom windows and dockable panes.
    PIDockingFrameworkBase piDockingFrameworkBase = new PIDockingFrameworkBase();
    FxDockFramework.setFrameworkBase( piDockingFrameworkBase );
    FxDockFramework.setGenerator(new PIDockingGenerator());

    initSettings();
    setStylesheet();
    PiRail.getInstance(); // initialize PI-Rail

    // load saved layout
    int layoutWindowCount = FxDockSchema.getWindowCount();

    // when no saved layout exists, open the first window
    PIDockingDialog mainDialog = new PIRailMainDialog( initialDialogID, initialContext, platform, createInitialParamList(), null );
    rootDlgCtrl = mainDialog.getController();
    piDockingFrameworkBase.registerWindow( mainDialog );

    // If there is a layout we need to restore the main window separate because of the menu.
    if(layoutWindowCount > 0)
    {
      String prefix = FxDockSchema.windowID(0);

      FxDockSchema.restoreWindow(prefix, mainDialog);
      // Register the first window by hand.

      Node content = FxDockSchema.loadLayout(prefix);
      mainDialog.setContent( content );

      LocalSettings settings = LocalSettings.find(mainDialog);
      if(settings != null)
      {
        String k = prefix + FxDockSchema.SUFFIX_BINDINGS;
        settings.loadValues(k);
      }

      FxDockSchema.loadContentSettings(prefix, mainDialog.getContent());

      FxDockFramework.loadLayout();
    }

    mainDialog.show();
    mainDialog.toFront();

  }

  private void initSettings() {
    // init non-ui subsystems


    File appDir = new File( DesktopSystem.getPathAppData(), SystemManager.getInstance().getProgramName() );
    appDir.mkdirs();
    File settingsFile = new File( appDir, SETTINGS_CONF );

    // If there is no previously saved file, copy our default from resources.
    if (!settingsFile.getAbsoluteFile().exists()) {
      try {
        Files.copy( getClass().getResource(  "/pi-rail.default.conf").openStream(), settingsFile.toPath());
      }
      catch (IOException e) {
        throw new RuntimeException( e );
      }

    }

    GlobalSettings.setFileProvider( settingsFile );
  }

  private void setStylesheet() {
    String defaultThemeFile = getClass().getResource("/css/atlanta/primer-light.css").toExternalForm();
    String savedCss = GlobalSettings.getString( "pi-rail-css" );

    if (Helper.isNotNullAndNotEmpty( savedCss )) {
      URL savedCssFileUrl = getClass().getResource( GlobalSettings.getString( "pi-rail-css" ) );

      if (savedCssFileUrl != null) {
        setUserAgentStylesheet(savedCssFileUrl.toExternalForm());
      }
      else {
        setUserAgentStylesheet(defaultThemeFile);
      }
    }
    else {
      setUserAgentStylesheet(defaultThemeFile);
    }
  }


  public static void main( String[] args ) {
    args = new String[1];
    args[0] = "Application";

    launch( args );
  }
}
