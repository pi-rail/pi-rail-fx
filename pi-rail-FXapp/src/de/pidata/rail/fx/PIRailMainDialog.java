package de.pidata.rail.fx;

import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.docking.PIDockingDialog;
import de.pidata.gui.javafx.FXDialogManager;
import de.pidata.gui.javafx.FXPlatform;
import de.pidata.models.tree.Context;
import de.pidata.qnames.QName;
import de.pidata.service.base.ParameterList;
import goryachev.common.util.GlobalSettings;
import goryachev.fx.OnWindowClosing;
import goryachev.fx.internal.LocalSettings;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

import static javafx.application.Application.getUserAgentStylesheet;

public class PIRailMainDialog extends PIDockingDialog {

  public PIRailMainDialog( QName initialDialogID, Context initialContext, FXPlatform platform, ParameterList initialParamList, DialogController parentDlgCtrl ) throws Exception {
    super();

    try {
      this.dialogID = initialDialogID;
      this.getIcons().add( new Image( getClass().getResource( "/drawable/clever_train_control_App_Icon_152px.png" ).toString() ) );

      load();

      setDialogID( dialogID.getName() );
      LocalSettings.get(this).add( DIALOG_ID, dialogIdProperty());
    }
    catch (IOException e) {
      throw new RuntimeException( e );
    }

    // Set initial with and height. Might be overridden by loading layout.
    setWidth( 1000 );
    setHeight( 800 );

    dialogController = platform.getControllerBuilder().createDialogController( initialDialogID, initialContext, this, parentDlgCtrl );
    if (initialParamList != null) {
      dialogController.setParameterList( initialParamList );
    }

    dialogController.activate( this );
  }

  public void load() throws IOException {
    URL mainDlgRes = getClass().getResource( "/layout/"+dialogID.getName()+".fxml" );
    System.out.println("Load FXML: " + "/layout/"+dialogID.getName()+".fxml");
    setMainContent( FXMLLoader.load( mainDlgRes ));
  }

  /*
  * Override the initial loaded FXML dialog (has only the menubar) to be set to top in the main BorderPane
  * */
  @Override
  protected void setMainContent( Node node) {
    setTop( node );
  }

  /**
   * Override to ask the user to confirm closing of window.
   * Make sure to check if the argument already has the user's choice and
   * perform the necessary action.
   * If a dialog must be shown, make sure to call toFront().
   *
   * @param choice
   */
  @Override
  public void confirmClosing( OnWindowClosing choice ) {
    if ( !askForClosing() ) {
      choice.setCancelled();
    }
    else {
      saveLayout();
      String currentStyleSheet = getUserAgentStylesheet();
      currentStyleSheet = currentStyleSheet.substring( currentStyleSheet.indexOf( "/css" ) );
      GlobalSettings.setString( "pi-rail-css", currentStyleSheet );
      GlobalSettings.save();

      dialogController.close( true );

      Platform.exit();
      System.exit(0);
    }
  }

  private boolean askForClosing() {
    Alert alert = new Alert( Alert.AlertType.CONFIRMATION );
    alert.setTitle( "Beenden" );
    alert.setHeaderText( "Wollen Sie die App wirklich beenden?" );
    Optional<ButtonType> result = alert.showAndWait();
    return result.get() == ButtonType.OK;
  }
}
