/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.configurator;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.controller.file.FileChooserParameter;
import de.pidata.gui.controller.file.FileChooserResult;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.AbstractEditConfig;
import de.pidata.rail.railway.RailDevice;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.stream.StreamHelper;
import de.pidata.string.Helper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.*;
import java.net.InetAddress;
import java.util.Properties;

public class UpdateFirmware extends GuiOperation {

  private String filePath;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TextEditorController outputCtrl = (TextEditorController) sourceCtrl.getDialogController().getController( NAMESPACE.getQName( "updaterOutput" ) );
    String firmwareSketch;
    this.filePath = null;
    if ((outputCtrl != null) && ("PI-Data".equals( outputCtrl.getStringValue() ))) {
      firmwareSketch = null;
    }
    else {
      RailDevice railDevice = (RailDevice) dataContext;
      firmwareSketch = railDevice.getFirmwareSketch();
    }
    String pattern = "*.bin";
    if (!Helper.isNullOrEmpty( firmwareSketch )) {
      pattern = firmwareSketch + pattern;
    }
    String firmwarePath;
    Storage storage = SystemManager.getInstance().getStorage( SystemManager.STORAGE_CLASSPATH, "." );
    if (storage.exists( "firmware" )) {
      firmwarePath = storage.getPath( "firmware" );
      FileChooserParameter fileChooserParameter = new FileChooserParameter( firmwarePath, pattern );
      String title = SystemManager.getInstance().getLocalizedMessage( "updateFirmwareSelect_H", null, null );
      sourceCtrl.getDialogController().showFileChooser( title, fileChooserParameter );
    }
    else {
      String classPath = storage.getPath( "firmware" );
      storage = SystemManager.getInstance().getStorage( "." );
      if (storage.exists( "firmware" )) {
        firmwarePath = storage.getPath( "firmware" );
        FileChooserParameter fileChooserParameter = new FileChooserParameter( firmwarePath, pattern );
        String title = SystemManager.getInstance().getLocalizedMessage( "updateFirmwareSelect_H", null, null );
        sourceCtrl.getDialogController().showFileChooser( title, fileChooserParameter );
      }
      else {
        Properties params = new Properties();
        String path = storage.getPath( "firmware" );
        File file1 = new File( classPath );
        params.put( "path", file1.getAbsolutePath() );
        File file2 = new File( path );
        Logger.info( "Firmware neither found at path=" + file1.getAbsolutePath() + " nor at path=" + file2.getAbsolutePath() );
        String title = SystemManager.getInstance().getLocalizedMessage( "updateFirmwareFwMissing_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "updateFirmwareFwMissing_TXT", null, params );
        showMessage( sourceCtrl, title, text );
      }
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof FileChooserResult) {
      if (resultOK) {
        this.filePath = ((FileChooserResult) resultList).getFilePath();
        if (!Helper.isNullOrEmpty( filePath )) {
          try {
            RailDevice railDevice = (RailDevice) parentDlgCtrl.getModel();
            String firmwareSketch = railDevice.getFirmwareSketch();
            if (!filePath.contains( firmwareSketch )) {
              String title = SystemManager.getInstance().getLocalizedMessage( "updateFirmwareWrongFw_H", null, null );
              String text = SystemManager.getInstance().getLocalizedMessage( "updateFirmwareWrongFw_TXT", null, null );
              String btnYes = SystemManager.getInstance().getGlossaryString( "upload" );
              String btnNo = SystemManager.getInstance().getGlossaryString( "cancel" );
              parentDlgCtrl.showQuestion( title, text, btnYes, btnNo, null );
            }
            else {
              doUpdateFirmware( parentDlgCtrl, railDevice, filePath );
              this.filePath = null;
            }
          }
          catch (Exception e) {
            Logger.error( "Error while starting firmware update", e );
          }
        }
      }
    }
    else if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      String title = SystemManager.getInstance().getLocalizedMessage( "updateFirmwareWrongFw_H", null, null );
      if (resultOK && result.getTitle().equals( title )) {
        if (!Helper.isNullOrEmpty( filePath )) {
          try {
            RailDevice railDevice = (RailDevice) parentDlgCtrl.getModel();
            doUpdateFirmware( parentDlgCtrl, railDevice, filePath );
            this.filePath = null;
          }
          catch (Exception e) {
            Logger.error( "Error while starting firmware update", e );
          }
        }
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }

  private void doUpdateFirmware( DialogController parentDlgCtrl, RailDevice railDevice, String filePath ) throws IOException {
    InetAddress deviceAddress = railDevice.getAddress().getInetAddress();
    String addr = deviceAddress.getHostAddress();
    AbstractEditConfig delegate = (AbstractEditConfig) parentDlgCtrl.getDelegate();
    final TextController outputCtrl = delegate.getUpdaterOutput();
    //--- Determine target (ESP32 or ESP8266/ESP8285)
    String espTool;
    int pos = filePath.lastIndexOf( '/' );
    String fileName;
    if (pos > 0) {
      fileName = filePath.substring( pos );
    }
    else {
      fileName = filePath;
    }
    Storage storage = SystemManager.getInstance().getStorage( SystemManager.STORAGE_CLASSPATH, "firmware" );
    if (fileName.contains( "ESP32" )) {
      espTool = storage.getPath( "espota_esp32.py" );
    }
    else {
      espTool = storage.getPath( "espota_esp8266.py" );
    }
    //--- Execute command
    String python;
    String os = System.getProperty("os.name").toLowerCase();
    if ((os.indexOf( "linux" ) >= 0) || (os.indexOf( "mac os" ) >= 0)) {
      python = "python3";
    }
    else {
      python = "python";
    }
    final StringBuilder builder = delegate.getMessageCollector();
    try {
      ProcessBuilder pb = new ProcessBuilder( python, espTool, "-d", "-i", addr, "-f", filePath );
      Process process = pb.start();
      //--- Get output stream to write from it
      final InputStream error = process.getErrorStream();
      Thread inputThread = new Thread( new Runnable() {
        public void run() {
          InputStreamReader errReader = null;
          try {
            builder.append( "----------\n" );
            errReader = new InputStreamReader( error );
            int ch = errReader.read();
            while (ch >= 0) {
              builder.append( (char) ch );
              outputCtrl.setValue( builder.toString() );
              ch = errReader.read();
            }
          }
          catch (Exception e) {
            Logger.error( "Error while firmware update", e );
            builder.append( "ERROR: " ).append( e.getMessage() ).append( "\n" );
          }
          finally {
            builder.append( "----------\n" );
            outputCtrl.setValue( builder.toString() );
            StreamHelper.close( error );
          }
        }
      } );
      inputThread.start();
    }
    catch (Exception e) {
      Logger.error( "Error while firmware update", e );
      builder.append( "ERROR: " ).append( e.getMessage() ).append( "\n" );
      outputCtrl.setValue( builder.toString() );
    }
  }
}
