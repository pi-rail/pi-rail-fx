@echo off
For /F "tokens=1* delims==" %%A IN (./res/version.properties) DO (
    IF "%%A"=="application.version" set VERSION=%%B
)

jpackage --input ./bin/ --name CTC-App --app-version %VERSION% --main-jar CTC-Desktop.jar --win-shortcut --win-menu --win-menu-group  "CTC-System" --icon ctc-icon.ico --vendor "CTC-System.de" --type msi