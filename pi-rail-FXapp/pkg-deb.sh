#!/bin/bash

file="./res/version.properties"
function prop {
    grep "${1}" ${file} | cut -d'=' -f2
}
VERSION=$(prop 'application.version')

~/java/jdk-21.0.5-full/bin/jpackage --input ./bin/ --name CTC-App --app-version $VERSION --main-jar CTC-Desktop.jar --linux-shortcut --linux-menu-group "Game" --icon ctc-icon.png --vendor "CTC-System.de" --type deb